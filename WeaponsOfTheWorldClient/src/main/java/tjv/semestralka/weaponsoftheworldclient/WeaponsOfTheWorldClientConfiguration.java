package tjv.semestralka.weaponsoftheworldclient;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import tjv.semestralka.weaponsoftheworldclient.domain.Army;
import tjv.semestralka.weaponsoftheworldclient.domain.Gun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.NewArmy;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.NewGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.SimpleGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.UpdateGun;

@Configuration
public class WeaponsOfTheWorldClientConfiguration {
    @Bean
    public ModelMapper modelMapperBean(){
        var modelMapper = new ModelMapper();
        modelMapper.createTypeMap(NewGun.class, Gun.class);
        modelMapper.createTypeMap(Gun.class, SimpleGun.class);
        modelMapper.createTypeMap(NewArmy.class, Army.class);
        modelMapper.createTypeMap(UpdateGun.class, Gun.class);
        return modelMapper;
    }

}
