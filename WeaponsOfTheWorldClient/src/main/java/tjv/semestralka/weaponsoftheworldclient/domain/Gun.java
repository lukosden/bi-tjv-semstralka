package tjv.semestralka.weaponsoftheworldclient.domain;


import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class Gun implements EntityWithId<Long>{
    private Long id;
    private String name;
    private Long generation;
    private Long rpm;
    private float weight;
    private Manufacturer manufacturer;
    private Set<Army> armies = new HashSet<>();
    @Override
    public Long getId() {
        return id;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Gun g)) {
            return false;
        }
        return id.equals(g.id);
    }
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
