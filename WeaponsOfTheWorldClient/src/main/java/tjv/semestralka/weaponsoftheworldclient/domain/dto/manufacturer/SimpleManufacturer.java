package tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleManufacturer {
    private Long id;
    private String name;
}
