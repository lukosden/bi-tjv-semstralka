package tjv.semestralka.weaponsoftheworldclient.domain.dto.army;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewArmy {
    private String country;
    private Long size;
}
