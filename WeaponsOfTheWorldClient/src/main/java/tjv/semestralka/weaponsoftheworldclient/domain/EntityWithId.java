package tjv.semestralka.weaponsoftheworldclient.domain;

public interface EntityWithId<ID> {
    ID getId();
}
