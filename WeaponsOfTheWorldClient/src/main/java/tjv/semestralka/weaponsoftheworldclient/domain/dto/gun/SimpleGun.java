package tjv.semestralka.weaponsoftheworldclient.domain.dto.gun;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleGun {
    private Long id;
    private String name;
}
