package tjv.semestralka.weaponsoftheworldclient.domain.dto.army;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleArmy {
    private Long id;
    private String country;
}
