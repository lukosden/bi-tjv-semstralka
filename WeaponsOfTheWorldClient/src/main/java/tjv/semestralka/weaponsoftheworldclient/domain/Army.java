package tjv.semestralka.weaponsoftheworldclient.domain;

import lombok.Getter;
import lombok.Setter;


import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class Army implements EntityWithId<Long>{
    private Long id;
    private String country;
    private Long size;
    private Set<Gun> guns = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Army a)) {
            return false;
        }
        return id.equals(a.id);
    }
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
