package tjv.semestralka.weaponsoftheworldclient.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import tjv.semestralka.weaponsoftheworldclient.api.client.GunClient;
import tjv.semestralka.weaponsoftheworldclient.domain.Gun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.NewGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.SimpleGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.UpdateGun;

import java.util.Collection;

@Service
public class GunService {
    GunClient gunClient;
    ModelMapper mapper;

    public GunService(GunClient gunClient, ModelMapper mapper) {
        this.gunClient = gunClient;
        this.mapper = mapper;
    }


    public Collection<SimpleGun> getAllGuns() {
        return gunClient.readAll();
    }

    public Gun getGunById(Long id) {
        return gunClient.readGun(id);
    }

    public void deleteGunById(Long id) {
        gunClient.deleteGun(id);
    }

    public Gun updateGunById(Long id, UpdateGun gunUpdate) {
        gunUpdate.setManufacturer(getGunById(id).getManufacturer().getId());
        return gunClient.updateGun(id, gunUpdate);
    }

    public Gun updateGunManufacturer(Long gunId, Long manufacturerId) {
        return gunClient.updateGunManufacturer(gunId, manufacturerId);
    }
    public void createGun(NewGun gun){
        gunClient.createGun(gun);
    }
}
