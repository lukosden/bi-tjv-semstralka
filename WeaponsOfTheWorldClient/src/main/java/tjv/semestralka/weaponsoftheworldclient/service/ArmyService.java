package tjv.semestralka.weaponsoftheworldclient.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import tjv.semestralka.weaponsoftheworldclient.api.client.ArmyClient;
import tjv.semestralka.weaponsoftheworldclient.api.client.GunClient;
import tjv.semestralka.weaponsoftheworldclient.api.client.ManufacturerClient;
import tjv.semestralka.weaponsoftheworldclient.domain.Army;
import tjv.semestralka.weaponsoftheworldclient.domain.Gun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.NewArmy;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.SimpleArmy;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.UpdateArmy;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.SimpleManufacturer;

import java.util.Collection;

@Service
public class ArmyService{
    ArmyClient armyClient;
    GunClient gunClient;
    ManufacturerClient manufacturerClient;
    ModelMapper mapper;
    public ArmyService(ArmyClient armyClient, GunClient gunClient, ManufacturerClient manufacturerClient, ModelMapper mapper) {
        this.armyClient = armyClient;
        this.gunClient = gunClient;
        this.manufacturerClient = manufacturerClient;
        this.mapper = mapper;
    }
    public Collection<SimpleArmy> getAllArmies(){
        return armyClient.readAll();
    }

    public Army getArmyById(Long id) {
        return armyClient.readArmy(id);
    }

    public void deleteArmyById(Long id) {
        armyClient.deleteArmy(id);
    }

    public void deleteGunById(Long armyId, Long gunId) {
        armyClient.deleteGun(armyId, gunId);
    }

    public Army addGunById(Long armyId, Long gunId) {
        Army army = getArmyById(armyId);
        Gun gun = gunClient.readGun(gunId);
        if (army.getGuns().contains(gun)) {
            throw new IllegalArgumentException("Army from " + army.getCountry() + " already has gun with id " + gunId);
        }


        if (!gun.getManufacturer().getCountry().equals(army.getCountry())
                && checkIfSameCountryManufacturerExists(army)) {
            long same = armyClient.getSameCountryGunsCount(armyId);
            long different = army.getGuns().size() - same;

            if (same <= different)
                throw new IllegalArgumentException("Army from " + army.getCountry() + " already has " + (different) + " guns from different country which is current maximum");

        }

        return armyClient.addGun(armyId, gunId);
    }

    private boolean checkIfSameCountryManufacturerExists(Army army) {
        for (SimpleManufacturer man : manufacturerClient.readAll()) {
            if(manufacturerClient.readManufacturer(man.getId()).getCountry().equals(army.getCountry()))
                return true;
        }
        return false;
    }

    public Army updateArmyById(Long id, Army armyUpdate){
        return armyClient.updateArmy(id, mapper.map(armyUpdate, UpdateArmy.class));
    }

    public void createArmy(NewArmy army) {
        armyClient.createArmy(army);
    }
}
