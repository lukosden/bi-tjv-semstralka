package tjv.semestralka.weaponsoftheworldclient.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import tjv.semestralka.weaponsoftheworldclient.api.client.GunClient;
import tjv.semestralka.weaponsoftheworldclient.api.client.ManufacturerClient;
import tjv.semestralka.weaponsoftheworldclient.domain.Manufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.Gun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.SimpleGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.NewManufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.UpdateManufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.SimpleManufacturer;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class ManufacturerService {
    ManufacturerClient manufacturerClient;
    GunClient gunClient;
    ModelMapper mapper;

    public ManufacturerService(ManufacturerClient manufacturerClient, GunClient gunClient, ModelMapper mapper) {
        this.manufacturerClient = manufacturerClient;
        this.gunClient = gunClient;
        this.mapper = mapper;
    }

    public Collection<SimpleManufacturer> getAllManufacturers(){
        return manufacturerClient.readAll();
    }

    public Manufacturer getManufacturerById(Long id) {
        Collection<SimpleGun> guns = gunClient.readAll();
        Manufacturer manufacturer = manufacturerClient.readManufacturer(id);
        Set<Gun> madeGuns = new HashSet<>();
        for(SimpleGun g : guns){
            Gun gun = gunClient.readGun(g.getId());
            if(gun.getManufacturer().getId().equals(id)){
                madeGuns.add(gun);
            }
        }
        manufacturer.setGuns(madeGuns);
        return manufacturer;
    }

    public void deleteManufacturerById(Long id) {
        manufacturerClient.deleteManufacturer(id);
    }

    public Manufacturer updateManufacturerById(Long id, Manufacturer manufacturerUpdate){
        return manufacturerClient.updateManufacturer(id, mapper.map(manufacturerUpdate, UpdateManufacturer.class));
    }

    public void createManufacturer(NewManufacturer manufacturer) {
        manufacturerClient.createManufacturer(manufacturer);
    }
}
