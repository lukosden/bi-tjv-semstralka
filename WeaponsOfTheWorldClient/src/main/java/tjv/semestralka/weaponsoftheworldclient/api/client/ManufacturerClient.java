package tjv.semestralka.weaponsoftheworldclient.api.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestralka.weaponsoftheworldclient.domain.Manufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.NewManufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.UpdateManufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.SimpleManufacturer;

import java.util.Arrays;
import java.util.Collection;

@Component
public class ManufacturerClient {
    private final RestClient manufacturerRestClient;

    public ManufacturerClient(@Value("${api.url}")String baseUrl){
        manufacturerRestClient = RestClient.create(baseUrl + "/manufacturers");
    }

    public Collection<SimpleManufacturer> readAll(){
        return Arrays.asList(manufacturerRestClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(SimpleManufacturer[].class)
                .getBody()
        );
    }
    public Manufacturer readManufacturer(Long id) {
        return manufacturerRestClient.get()
                .uri("/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Manufacturer with id " + id + " not found");})
                .toEntity(Manufacturer.class)
                .getBody();
    }
    public void deleteManufacturer(Long id) {
        manufacturerRestClient.delete()
                .uri("/{id}", id)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Manufacturer with id " + id + " not found");})
                .toBodilessEntity();
    }

    public void deleteGun(Long manufacturerId, Long gunId) {
        manufacturerRestClient.delete()
                .uri("/{manufacturerId}/guns/{gunId}", manufacturerId, gunId)
                .retrieve()
                .toBodilessEntity();
    }

    public Manufacturer addGun(Long manufacturerId, Long gunId) {
        return manufacturerRestClient.post()
                .uri("/{manufacturerId}/guns/{gunId}", manufacturerId, gunId)
                .contentType(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gun with id " + gunId + " not found");})
                .onStatus(status -> status.value() == 409, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "Manufacturer with id " + manufacturerId + " already has gun with id " + gunId);})
                .body(Manufacturer.class);
    }

    public long getSameCountryGunsCount(Long id) {
        return manufacturerRestClient.get()
                .uri("/{id}/guns-from-same-country", id)
                .retrieve()
                .body(long.class);
    }
    public Manufacturer updateManufacturer(Long id, UpdateManufacturer manufacturerUpdate){
        return manufacturerRestClient.put()
                .uri("/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(manufacturerUpdate)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Manufacturer with id " + id + " not found");})
                .onStatus(status -> status.value() == 409, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT,"Manufacturer named " + manufacturerUpdate.getName() + " already exists");})
                .body(Manufacturer.class);
    }

    public void createManufacturer(NewManufacturer manufacturer) {
        manufacturerRestClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .body(manufacturer)
                .retrieve()
                .onStatus(status -> status.value() == 409, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "Manufacturer named " + manufacturer.getName() + " already exists");})
                .body(Manufacturer.class);
    }
}
