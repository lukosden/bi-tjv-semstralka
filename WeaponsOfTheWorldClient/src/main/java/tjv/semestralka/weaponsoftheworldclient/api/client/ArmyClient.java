package tjv.semestralka.weaponsoftheworldclient.api.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestralka.weaponsoftheworldclient.domain.Army;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.NewArmy;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.SimpleArmy;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.UpdateArmy;

import java.util.Arrays;
import java.util.Collection;

@Component
public class ArmyClient {
    private final RestClient armyRestClient;

    public ArmyClient(@Value("${api.url}") String baseUrl){
        armyRestClient = RestClient.create(baseUrl + "/armies");
    }

    public Collection<SimpleArmy> readAll(){
        return Arrays.asList(armyRestClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(SimpleArmy[].class)
                .getBody()
        );
    }

    public Army readArmy(Long id) {
        return armyRestClient.get()
                .uri("/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(Army.class)
                .getBody();
    }

    public void deleteArmy(Long id) {
        armyRestClient.delete()
                .uri("/{id}", id)
                .retrieve()
                .toBodilessEntity();
    }

    public void deleteGun(Long armyId, Long gunId) {
        armyRestClient.delete()
                .uri("/{armyId}/guns/{gunId}", armyId, gunId)
                .retrieve()
                .toBodilessEntity();
    }

    public Army addGun(Long armyId, Long gunId) {
        return armyRestClient.post()
                .uri("/{armyId}/guns/{gunId}", armyId, gunId)
                .contentType(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gun with id " + gunId + " not found");})
                .onStatus(status -> status.value() == 409, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "Army with id " + armyId + " already has gun with id " + gunId);})
                .body(Army.class);
    }

    public long getSameCountryGunsCount(Long id) {
        return armyRestClient.get()
                .uri("/{id}/guns-from-same-country", id)
                .retrieve()
                .body(long.class);
    }
    public Army updateArmy(Long id, UpdateArmy armyUpdate){
        return armyRestClient.put()
                .uri("/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(armyUpdate)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Army with id " + id + " not found");})
                .onStatus(status -> status.value() == 409, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, armyUpdate.getCountry() + " already has an army");})
                .body(Army.class);
    }

    public void createArmy(NewArmy army) {
        armyRestClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .body(army)
                .retrieve()
                .onStatus(status -> status.value() == 409, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, army.getCountry() + " already has an army");})
                .body(Army.class);
    }
}
