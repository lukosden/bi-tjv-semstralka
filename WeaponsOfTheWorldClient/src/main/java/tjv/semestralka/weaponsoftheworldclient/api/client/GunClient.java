package tjv.semestralka.weaponsoftheworldclient.api.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestralka.weaponsoftheworldclient.domain.Gun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.NewGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.SimpleGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.UpdateGun;

import java.util.Arrays;
import java.util.Collection;

@Component
public class GunClient {
    private final RestClient gunRestClient;

    public GunClient(@Value("${api.url}") String baseUrl){
        gunRestClient = RestClient.create(baseUrl + "/guns");
    }

    public Gun readGun(Long id){
        return gunRestClient.get()
                .uri("/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gun with id " + id + " not found");})
                .toEntity(Gun.class)
                .getBody();

    }
    public Collection<SimpleGun> readAll(){
        return Arrays.asList(gunRestClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(SimpleGun[].class)
                .getBody()
        );
    }

    public void deleteGun(Long id) {
        gunRestClient.delete()
                .uri("/{id}", id)
                .retrieve()
                .toBodilessEntity();
    }

    public Gun updateGun(Long id, UpdateGun gunUpdate){
        return gunRestClient.put()
                .uri("/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(gunUpdate)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gun with id " + id + " not found");})
                .body(Gun.class);
    }

    public NewGun createGun(NewGun gun) {
        return gunRestClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .body(gun)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Manufacturer with id " + gun.getManufacturer() + " not found");})
                .body(NewGun.class);
    }

    public Gun updateGunManufacturer(Long gunId, Long manufacturerId) {
        return gunRestClient.put()
                .uri("/{id}/{manufacturerId}", gunId, manufacturerId)
                .contentType(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(status -> status.value() == 404, (request, response) -> {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Manufacturer with id " + manufacturerId + " not found");})
                .body(Gun.class);
    }
}
