package tjv.semestralka.weaponsoftheworldclient.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestralka.weaponsoftheworldclient.domain.Gun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.NewGun;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.gun.UpdateGun;
import tjv.semestralka.weaponsoftheworldclient.service.GunService;


@Controller
@RequestMapping("guns")
public class GunController {
    private final GunService gunService;

    public GunController(GunService gunService) {
        this.gunService = gunService;
    }

    @GetMapping
    public String getAllGuns(Model model){
        model.addAttribute("guns", gunService.getAllGuns());
        return "guns";
    }

    @GetMapping("/{id}")
    public String getGun(@PathVariable Long id, Model model) {
        Gun gun = gunService.getGunById(id);
        model.addAttribute("gun", gun);
        model.addAttribute("manufacturer", gun.getManufacturer());
        return "gun";
    }
    @DeleteMapping("/{id}")
    public String deleteGun(@PathVariable Long id, Model model) {
        gunService.deleteGunById(id);
        model.addAttribute("guns", gunService.getAllGuns());
        return "guns";
    }

    @PutMapping("/{gunId}/manufacturer-update")
    public String changeManufacturer(@PathVariable Long gunId, @RequestParam Long manufacturerId, Model model) {
        Gun gun;
        try{
            gun = gunService.updateGunManufacturer(gunId, manufacturerId);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("gun", gun);
        model.addAttribute("manufacturer", gun.getManufacturer());
        return "gun";
    }
    @PutMapping("/{id}")
    public String updateGun(@PathVariable Long id, UpdateGun gunUpdate, Model model){
        Gun gun;
        try{
            gun = gunService.updateGunById(id, gunUpdate);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("gun", gun);
        model.addAttribute("manufacturer", gun.getManufacturer());
        return "gun";
    }
    @RequestMapping("/{id}/update")
    public String showGunUpdateForm(@PathVariable Long id, Model model) {
        Gun gun = gunService.getGunById(id);
        model.addAttribute("gun", gun);
        return "gun-update";
    }

    @PostMapping()
    public String createGun(NewGun gun, Model model){
        try{
            gunService.createGun(gun);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("guns", gunService.getAllGuns() );
        return "guns";
    }
    @RequestMapping("/create")
    public String showCreateGunForm(Model model){
        NewGun gun = new NewGun();
        model.addAttribute("gun", gun);
        return "gun-create";
    }

}