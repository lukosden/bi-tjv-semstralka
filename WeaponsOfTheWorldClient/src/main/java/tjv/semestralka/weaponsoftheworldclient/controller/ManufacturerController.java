package tjv.semestralka.weaponsoftheworldclient.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestralka.weaponsoftheworldclient.domain.Manufacturer;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.manufacturer.NewManufacturer;
import tjv.semestralka.weaponsoftheworldclient.service.ManufacturerService;


@Controller
@RequestMapping("manufacturers")
public class ManufacturerController {
    private final ManufacturerService manufacturerService;

    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public String getAllManufacturers(Model model){
        model.addAttribute("manufacturers", manufacturerService.getAllManufacturers());
        return "manufacturers";
    }

    @GetMapping("/{id}")
    public String getManufacturer(@PathVariable Long id, Model model) {
        try{
            model.addAttribute("manufacturer", manufacturerService.getManufacturerById(id));
        }
        catch (ResponseStatusException e) {
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        return "manufacturer";
    }
    @DeleteMapping("/{id}")
    public String deleteManufacturer(@PathVariable Long id, Model model) {
        try{
            manufacturerService.deleteManufacturerById(id);
        }
        catch (ResponseStatusException e) {
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("manufacturers", manufacturerService.getAllManufacturers());
        return "manufacturers";
    }


    @PutMapping("/{id}")
    public String updateManufacturer(@PathVariable Long id, Manufacturer manufacturerUpdate, Model model){
        Manufacturer manufacturer;
        try{
            manufacturer = manufacturerService.updateManufacturerById(id, manufacturerUpdate);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("manufacturer", manufacturer );
        return "manufacturer";
    }
    @RequestMapping("/{id}/update")
    public String showManufacturerUpdateForm(@PathVariable Long id, Model model) {
        Manufacturer manufacturer = manufacturerService.getManufacturerById(id);
        model.addAttribute("manufacturer", manufacturer);
        return "manufacturer-update";
    }

    @PostMapping()
    public String createManufacturer(NewManufacturer manufacturer, Model model){
        try{
            manufacturerService.createManufacturer(manufacturer);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("manufacturers", manufacturerService.getAllManufacturers() );
        return "manufacturers";
    }
    @RequestMapping("/create")
    public String showCreateManufacturerForm(Model model){
        NewManufacturer manufacturer = new NewManufacturer();
        model.addAttribute("manufacturer", manufacturer);
        return "manufacturer-create";
    }

}