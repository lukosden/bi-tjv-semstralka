package tjv.semestralka.weaponsoftheworldclient.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestralka.weaponsoftheworldclient.domain.Army;
import tjv.semestralka.weaponsoftheworldclient.domain.dto.army.NewArmy;
import tjv.semestralka.weaponsoftheworldclient.service.ArmyService;


@Controller
@RequestMapping("armies")
public class ArmyController {
    private final ArmyService armyService;

    public ArmyController(ArmyService armyService) {
        this.armyService = armyService;
    }

    @GetMapping
    public String getAllArmies(Model model){
        model.addAttribute("armies", armyService.getAllArmies());
        return "armies";
    }

    @GetMapping("/{id}")
    public String getArmy(@PathVariable Long id, Model model) {
        model.addAttribute("army", armyService.getArmyById(id));
        return "army";
    }
    @DeleteMapping("/{id}")
    public String deleteArmy(@PathVariable Long id, Model model) {
        armyService.deleteArmyById(id);
        model.addAttribute("armies", armyService.getAllArmies());
        return "armies";
    }
    @DeleteMapping("/{armyId}/guns/{gunId}")
    public String deleteGun(@PathVariable Long armyId, @PathVariable Long gunId, Model model) {
        armyService.deleteGunById(armyId, gunId);
        model.addAttribute("army", armyService.getArmyById(armyId));
        return "army";
    }
    @PostMapping("/{armyId}/guns")
    public String addGun(@PathVariable Long armyId, @RequestParam Long gunId, Model model) {
        Army army;
        try{
            army = armyService.addGunById(armyId, gunId);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        catch (IllegalArgumentException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("army", army);
        return "army";
    }
    @PutMapping("/{id}")
    public String updateArmy(@PathVariable Long id, Army armyUpdate, Model model){
        Army army;
        try{
            army = armyService.updateArmyById(id, armyUpdate);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("army", army );
        return "army";
    }
    @RequestMapping("/{id}/update")
    public String showArmyUpdateForm(@PathVariable Long id, Model model) {
        Army army = armyService.getArmyById(id);
        model.addAttribute("army", army);
        return "army-update";
    }

    @PostMapping()
    public String createArmy(NewArmy army, Model model){
        try{
            armyService.createArmy(army);
        }
        catch (ResponseStatusException e){
            model.addAttribute("error", e.getMessage());
            return "dialog";
        }
        model.addAttribute("armies", armyService.getAllArmies() );
        return "armies";
    }
    @RequestMapping("/create")
    public String showCreateArmyForm(Model model){
        NewArmy army = new NewArmy();
        model.addAttribute("army", army);
        return "army-create";
    }

}