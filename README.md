Serverová část obsahuje docker compose, který spustí databázi na portu 5432, pokud by bylo potřeba jej změnit, udělejte tak i v
serverovém application.properties v spring.datasource.url=jdbc:postgresql://localhost:5432/wotw_db?currentSchema=public

Samotny server je nastaveny na 8080 v application.properties, pokud by ten bylo potřeba změnit, tak bude potřeba ještě tento port změnit v klientovi v application.properties v api.url=http://localhost:8080

Klient beží na 8081, ten se dá bez problému změnit

Pokud nebudou provedeny změny portu serveru, tak dokumentace bude dostupná na http://localhost:8080/swagger-ui/index.html

Pokud nebudou provedeny změny portu klienta, tak UI bude dostupné na http://localhost:8081/

Spuštění -  1. spustit docker compose up  a počkat odkud se databáze nespustí
            2. spustit serverovou část a počkat až se aplikace spustí
            3. libovolným způsobem spustit dodaný insert script nad vytvořenou databází
            4. spustit klientskou část
